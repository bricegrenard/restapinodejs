const fetch = require('node-fetch');
const express = require('express')
const app = express()

app.get('/getRandomValue', (req, res) =>
{
    var val = randomInt(1,100000)
    res.send(JSON.stringify({"val": val.toString()}))
});

app.listen(3000, () => console.log('App listening on port 3000!'))

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low)
  }